extern crate minifb;

mod line;
use line::*;

use minifb::{Key, WindowOptions, Window, Scale};

const WIDTH: usize = 100;
const HEIGHT: usize = 100;

fn main() {
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

    let mut window = Window::new("Test - ESC to exit",
                                 WIDTH,
                                 HEIGHT,
                                 WindowOptions{
                                     borderless: false,
                                     resize: false,
                                     scale: Scale::X8,
                                     title: true
                                 }
    ).unwrap();

    let mut li = LineIter { 
        start: Point { x: 10, y: 10 },
        end: Point { x: 100, y: 50 },
        x: 10,
        y: 10,
        fraction: 0,
        first: true,
     };

    while window.is_open() && !window.is_key_down(Key::Escape) {

        // draw one pixel per frame!
        if let Some(p) = li.next() {
            buffer[(p.y * WIDTH as i32 + p.x) as usize] = 255;
            println!("{:?}", p);
            std::thread::sleep_ms(25);
        }

        
        // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
        window.update_with_buffer(&buffer).unwrap();
    }
}