// simple 2d point
#[derive(Debug)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

// used by the iterator to store state
pub struct LineIter {
    pub start: Point,
    pub end: Point,
    pub x: i32,
    pub y: i32,
    pub fraction: i32,
    pub first: bool,
}

impl Iterator for LineIter {
    // we will be iterating along the line using a bresenham algorithm returning the point each step
    type Item = Point;

    // next() is the only required method
    fn next(&mut self) -> Option<Point> {
        let mut dx = self.end.x - self.start.x;
        let mut dy = self.end.y - self.start.y;
        let step_x: i32;
        let step_y: i32;
        
        // recalculate these every iteration because we are animating and don't care about speed
        if dx < 0 { dx = -dx; step_x = -1; } else { step_x = 1; }
        if dy < 0 { dy = -dy; step_y = -1; } else { step_y = 1; }
        dx *= 2;
        dy *= 2;

        // if first point just return start!
        if self.first {
            if dx > dy {
                self.fraction = 2 * dy - dx;
            } else {
                self.fraction = 2 * dx - dy;
            }
            self.first = false;
            return Some(Point {x: self.start.x, y: self.start.y})
        }

        if self.x != self.end.x && self.y != self.end.y {
            if dx > dy {
                if self.fraction >= 0 {
                    self.y += step_y;
                    self.fraction -= dx;
                }
                self.x += step_x;
                self.fraction += dy;
                Some(Point {x: self.x, y: self.y})
            } else {
                if self.fraction >= 0 {
                    self.x += step_x;
                    self.fraction -= dy;
                }
                self.y += step_y;
                self.fraction += dx;
                Some(Point {x: self.x, y: self.y})
            }
        } else {
            None
        }
    }
}